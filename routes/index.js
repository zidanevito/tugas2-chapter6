const router = require('express').Router()
// Controller
const Auth = require('./auth')
const Product = require('./products')
const Warehouse = require('./warehouses')

// API auth
router.use('/api/v1/auth/', Auth)

// API products
router.use('/api/v1/', Product)

// API warehouses
router.use('/api/v1/', Warehouse)

module.exports = router
