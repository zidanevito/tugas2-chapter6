const router = require('express').Router()

// controller
const Warehouse = require('../controller/warehouseController')

// API Warehouses
router.get('/warehouses', Warehouse.findWarehouses)
router.post('/warehouses', Warehouse.createWarehouse)
router.get('/warehouses/:id', Warehouse.findWarehouseById)

module.exports = router