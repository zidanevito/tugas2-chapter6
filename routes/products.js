const router = require('express').Router()

const Product = require('../controller/productController')

// middleware
const uploader = require('../middlewares/uploader')
const Authentication = require('../middlewares/authenticate')

router.post('/products', Authentication, uploader.single('image'), Product.createProduct)
router.get('/products', Product.findProducts)
router.get('/products/:id', Product.findProductById)
router.put('/products/:id', Product.updateProduct)
router.delete('/products/:id', Product.deleteProduct)

module.exports = router
